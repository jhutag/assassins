# What is Assassin? #

A multiplayer mobile game for the live-action game [assassin](https://en.wikipedia.org/wiki/Assassin_(game)).

### The Team ###

* Marcos Bottenbley
* Caroline Cevallos
* Rebecca Bushko
* Scotty Thoms

### The Game ###

![img](mockups/t3A-1.png)
![img](mockups/t3A-2.png)
![img](mockups/t3A-3.png)
![img](mockups/t3A-4.png)
![img](mockups/t3A-5.png)
![img](mockups/t3A-6.png)
![img](mockups/t3A-7.png)
![img](mockups/t3A-8.png)